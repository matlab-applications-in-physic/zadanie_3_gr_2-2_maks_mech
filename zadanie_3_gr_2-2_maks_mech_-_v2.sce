// Created on Scilab 6.0.0
// Licence: Beerware

clc()
clear()

// You must set folder destination to folder with your csv data
file_1 = "dane-pomiarowe_2019-10-08.csv";
file_2 = "dane-pomiarowe_2019-10-09.csv";
file_3 = "dane-pomiarowe_2019-10-10.csv";
file_4 = "dane-pomiarowe_2019-10-11.csv";
file_5 = "dane-pomiarowe_2019-10-12.csv";
file_6 = "dane-pomiarowe_2019-10-13.csv";
file_7 = "dane-pomiarowe_2019-10-14.csv";

// setting number of days
num_days = 7;

// reading files
for i = 1:num_days
    execstr('day_'+string(i)+'= csvRead(file_'+string(i)+','+char(39)+';'+char(39)+',[],'+char(39)+'string'+char(39)+')')
end

// COMMENT: execstr is powerful function, that can deal with inserting strings which are variable names, i.e. size must br given variable name not string but execstr get over it
// char(39) is '

// checking size of each matrix
for i = 1:num_days
    execstr('size_'+string(i)+'= size(day_'+string(i)+')')
end

// getting date from name of file
for x = 1:num_days
execstr(['    start = strindex(file_'+string(x)+','+char(39)+'.'+char(39)+')-10'
'    texting = strsplit(file_'+string(x)+')'
'    name_'+string(x)+' = texting(start)'
'    for j = 1:9'
'        name_'+string(x)+' = name_'+string(x)+' + texting(start+j)'
'    end'])
end
// searching columns with tag PM10
PM10 = 'PM10'; // PM10 is a text to find
for x = 1:num_days
execstr(['    for i = 1:size_'+string(x)+'(1)'
'        for j = 1:size_'+string(x)+'(2)'
'            k = strindex(day_'+string(x)+'(i,j),'+string(PM10)+')'
'            if k > 0 then'
'                column_'+string(x)+' = j'
'            end'
'        end'
'    end'])
end

// searching for maxmium in all files and saving informations in which file and which line of file was maximum
k = 0;
for i = 1:num_days
execstr(['    for j = 1:size_'+string(i)+'(1)'
'        k_new = day_'+string(i)+'(j,column_'+string(i)+')'
'        if strtod(k_new) > k then'
'            k = strtod(k_new)'
'            tag_line = j'
'            tag_day = i'
'        end'
'    end'])
end

// displaying maximum 
execstr('hour = day_'+string(tag_day)+'('+string(tag_line)+',1)')
disp('Maximum: '+string(k))
disp('Hour: '+string(hour))
execstr('disp('+char(39)+'Day: '+char(39)+'+name_'+string(tag_day)+')')
execstr('disp('+char(39)+'Analyzed time: '+char(39)+'+name_'+string(tag_day)+'+'+char(39)+' to '+char(39)+'+name_'+string(num_days)+')')

// creating file, writing in
fil90 = mopen('PMresults.dat','w+')
mputl('Maximum: '+string(k),fil90)
mputl('Hour: '+string(hour),fil90)
execstr('mputl('+char(39)+'Day: '+char(39)+'+name_'+string(tag_day)+',fil90)')
execstr('mputl('+char(39)+'Analyzed time: '+char(39)+'+name_'+string(tag_day)+'+'+char(39)+' to '+char(39)+'+name_'+string(num_days)+',fil90)')
mclose(fil90)

// calculating all polution at day/night
sunshine = 0;
nighttime = 0;
day_start = 6; // AM hour
day_end = 8; // PM hour
for i = 1:num_days
    for j = 2:25 // it's true only when knowing structure of file
        if j >= day_start+1 & j < day_end+13 then
            sun = 1;
        else
            sun = 0;
        end
execstr(['        if sun == 1 then'
'            sunshine = sunshine + strtod(day_'+string(i)+'(j,6))'
'        else'
'            nighttime = nighttime + strtod(day_'+string(i)+'(j,6))'
'        end'])
    end
end

// calculating averages of day/night polution
day_pol = round(100*sunshine/(num_days*(day_end+12-day_start)))/100
night_pol = round(100*nighttime/(num_days*(24-(day_end+12-day_start))))/100

// counting uncertinity
sum_u_d = 0;
sum_u_n = 0;
for i = 1:num_days
    for j = 2:25 // it's true only when knowing structure of file
        if j >= day_start+1 & j < day_end+13 then
            sun = 1;
        else
            sun = 0;
        end
execstr(['        if sun == 1 then'
'            u_d = (strtod(day_'+string(i)+'(j,6)) - day_pol)^2'
'            sum_u_d = sum_u_d + u_d'
'        else'
'            u_n = (strtod(day_'+string(i)+'(j,6)) - night_pol)^2'
'            sum_u_n = sum_u_n + u_n'
'        end'])
    end
end

uncertainity_day = round(100*sqrt(sum_u_d/(num_days*(day_end+12-day_start)))/sqrt(num_days*(day_end+12-day_start)))/100;
uncertainity_night = round(100*sqrt(sum_u_n/(num_days*(24-(day_end+12-day_start))))/sqrt(num_days*(24-(day_end+12-day_start))))/100;

// displayin valuables

disp('Polution at day: '+string(day_pol))
disp('Uncertainity: ' + string(uncertainity_day))
disp('Polution at night: '+string(night_pol))
disp('Uncertainity: ' + string(uncertainity_night))

// matching test

value = round(100*abs(day_pol - night_pol) / sqrt((uncertainity_night)^2 + (uncertainity_day)^2))/100;
if value < 2 then
    disp('There is no difference beetween night/day becouse value of test is lower than 2, and exactly is: '+string(value))
end
if value > 3 then
    disp('There is difference beetween night/day becouse value of test is greater than 3, and exactly is: '+string(value))
end
if value > 2 & value < 3 then
    disp('It'+char(39)+'s hard to say if there is difference beetween night/day becouse value of test is greater than 2 and lower than 3, and exactly is: '+string(value))
end

// opening file, writing in
fil90 = mopen('PMresults.dat','a+')
mputl('Polution at day: '+string(day_pol),fil90)
mputl('Uncertainity: ' + string(uncertainity_day),fil90)
mputl('Polution at night: '+string(night_pol),fil90)
mputl('Uncertainity: ' + string(uncertainity_night),fil90)
if value < 2 then
    mputl('There is no difference beetween night/day becouse value of test is lower than 2, and exactly is: '+string(value),fil90)
end
if value > 3 then
    mputl('There is difference beetween night/day becouse value of test is greater than 3, and exactly is: '+string(value),fil90)
end
if value > 2 & value < 3 then
    mputl('It'+char(39)+'s hard to say if there is difference beetween night/day becouse value of test is greater than 2 and lower than 3, and exactly is: '+string(value),fil90)
end
mclose(fil90)
